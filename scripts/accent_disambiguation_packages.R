# General data manipulation and plotting
library(tidyverse)

# Colors
library(RColorBrewer)

# Linear mixed effects models
library(lme4)

# Generalized additive models
library(mgcv)

# Computing AUC
library(pROC)
