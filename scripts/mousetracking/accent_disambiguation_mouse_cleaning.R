## READ IN DATA (MOUSETRACKING) ####
data_mouse = list.files(path = "data/mousetracking", full.names = T) %>%
  map(read.table, header=T, sep="\t") %>%
  bind_rows()


## CLEAN DATA ####
data_mouse_clean = data_mouse %>%
  # Combine with cleaned rt data
  inner_join(data_rt_noout) %>%
  # Add a column for time
  mutate(time = percentage * rt) %>%
  # Update x-coordinates to flip trajectories of pictures on right side
  mutate(x_coord_adj = x_coord - 720) %>%
  mutate(x_coord_new = if_else(screen_side == 1, -x_coord_adj, x_coord_adj)) %>%
  # Cutoff at 2 seconds
  mutate(x_coord_new = ifelse(time <= 2000, x_coord_new, NA))


## GET BREAKPOINT INFORMATION ####
# Make data frame of windows
subjects = distinct(data_mouse_clean, subID)

windows = data_frame(time_min = rep(seq(0, 1900, 10), dim(subjects)[1]),
                     time_max = rep(seq(100, 2000, 10), dim(subjects)[1])) %>%
  group_by(time_min) %>%
  mutate(subID = unique(subjects$subID)) %>%
  ungroup()

# Run models on data
data_mouse_models = full_join(data_mouse_clean, windows) %>%
  # For a given start and end point drop unneeded data
  filter(time > time_min) %>%
  filter(time <= time_max) %>%
  # Focus on important columns
  select(subID, pair, target, trial_type, item_type, block, trial_number, trial_number_full,
         time, time_min, time_max, x_coord_new) %>%
  # Nest information for model
  nest(-c(subID, pair, target, trial_type, item_type, block, trial_number, trial_number_full,
          time_min, time_max), .key = information) %>%
  # Run simple linear model
  mutate(model = map(information, function(df) lm(df$x_coord_new ~ df$time)))

# Extract coefficients of models
data_mouse_coefficients = data_mouse_models %>%
  # Save coefficents of model for intercept and time slope
  mutate(coefficients = map(model, "coefficients")) %>%
  mutate(estimate = "intercept,time_slope") %>%
  transform(estimate = strsplit(estimate, ",")) %>%
  # Drop unneeded columns
  select(-c(information, model)) %>%
  # Unnest data
  unnest()

# Keep only cuttoff break points
data_mouse_breakpoints = data_mouse_coefficients %>%
  # Drop intercept coeffients
  filter(estimate == "time_slope") %>%
  # From all coeffients less than 1 and filter out to first one per trial
  filter(coefficients >= 1) %>%
  group_by(subID, trial_number_full) %>%
  filter(time_min == min(time_min)) %>%
  ungroup()









