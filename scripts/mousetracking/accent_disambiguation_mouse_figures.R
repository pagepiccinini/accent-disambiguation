## ORGANIZE DATA ####
data_mouse_figs = data_mouse_clean %>%
  mutate(item_type = factor(item_type, levels=c("no_accent", "accent"),
                            labels=c("no replacement", "replacement")))

data_mouse_breakpoint_figs = data_mouse_breakpoints %>%
  mutate(item_type = factor(item_type, levels=c("no_accent", "accent"),
                            labels=c("no replacement", "replacement")))


## CHECK DISTRIBUTION OF BREAKPOINTS ####
mouse_breakpoint_hist.plot = ggplot(data_mouse_breakpoint_figs,
                                    aes(x = time_min, fill = item_type)) +
  facet_grid(block ~ trial_type) +
  geom_histogram() +
  xlab("Break point in ms") +
  scale_fill_manual(values=c(col_noaccent, col_accent)) +
  theme_classic() +
  theme(text = element_text(size=18),
        legend.position = "top",legend.title = element_blank())

mouse_breakpoint_hist.plot
#ggsave("figures/mousetracking/mouse_histogram.pdf", mouse_breakpoint_hist.plot, height = 7, width = 7, units = "in")

mouse_breakpoint_no0_hist.plot = ggplot(filter(data_mouse_breakpoint_figs, time_min !=0),
                                    aes(x = time_min, fill = item_type)) +
  facet_grid(block ~ trial_type) +
  geom_histogram() +
  xlab("Break point in ms") +
  scale_fill_manual(values=c(col_noaccent, col_accent)) +
  theme_classic() +
  theme(text = element_text(size=18),
        legend.position = "top",legend.title = element_blank())

mouse_breakpoint_no0_hist.plot
#ggsave("figures/mousetracking/mouse_no0_histogram.pdf", mouse_breakpoint_no0_hist.plot, height = 7, width = 7, units = "in")


## MAKE FULL CURVE FIGURES ####
# All items by all blocks
mouse_block.plot = ggplot(data_mouse_figs, aes(x = time, y = x_coord_new,
                                               color = item_type, linetype = trial_type)) +
  facet_wrap(~block) +
  geom_smooth() +
  scale_color_manual(values=c(col_noaccent, col_accent)) +
  ggtitle("Mouse Tracking by Block") +
  xlab("Time in ms") +
  ylab("Distance from incorrect item (in pixels)") +
  theme_classic() +
  theme(text = element_text(size=18),
        legend.position = "top",legend.title = element_blank())

mouse_block.plot
#ggsave("figures/mousetracking/mouse_block.pdf", mouse_block.plot, height = 7, width = 7, units = "in")


## MAKE BREAK POINT FIGURES ####
# Example trials
mouse_breakpoint_example.plot = ggplot(filter(data_mouse_figs, subID == "sub_01" & trial_number_full <= 9),
                                        aes(x = time, y = x_coord_new,
                                        color = item_type, shape = trial_type)) +
  facet_wrap(~trial_number_full) +
  geom_point() +
  geom_vline(data = filter(data_mouse_breakpoint_figs, subID == "sub_01" & trial_number_full <= 9),
             aes(xintercept = time_min), lwd = 1.5) +
  scale_color_manual(values=c(col_noaccent, col_accent)) +
  ggtitle("Example Mouse Tracking with Break point") +
  xlab("Time in ms") +
  ylab("Distance from incorrect item (in pixels)") +
  theme_classic() +
  theme(text = element_text(size=18),
        legend.position = "top",legend.title = element_blank())

mouse_breakpoint_example.plot
#ggsave("figures/mousetracking/mouse_breakpoint_example.pdf", mouse_breakpoint_example.plot, height = 7, width = 7, units = "in")

# All items by trial number
mouse_breakpoint_trialnum.plot = ggplot(data_mouse_breakpoint_figs,
                                        aes(x = trial_number_full, y = time_min, color = item_type)) +
  facet_wrap(~trial_type) +
  #geom_point() +
  geom_smooth() +
  # Add lines for blocks
  geom_vline(xintercept = 40) +
  geom_vline(xintercept = 80) +
  geom_vline(xintercept = 120) +
  # Add annotation for blocks
  annotate("text", x = 20, y = 600, label = "Block 1", size = 4) +
  annotate("text", x = 60, y = 600, label = "Block 2", size = 4) +
  annotate("text", x = 100, y = 600, label = "Block 3", size = 4) +
  annotate("text", x = 140, y = 600, label = "Block 4", size = 4) +
  scale_x_continuous(limits = c(0, 160), breaks = seq(0, 160, by = 20)) +
  scale_color_manual(values=c(col_noaccent, col_accent)) +
  ggtitle("Break points by Trial Number") +
  xlab("Trial number") +
  ylab("Break point (in ms)") +
  theme_classic() +
  theme(text = element_text(size=18),
        legend.position = "top",legend.title = element_blank())

mouse_breakpoint_trialnum.plot
#ggsave("figures/mousetracking/mouse_breakpoint_trialnum.pdf", mouse_breakpoint_trialnum.plot, height = 7, width = 7, units = "in")

# Look at trials with with break point at 0
mouse_breakpoint_0.plot = ggplot(filter(data_mouse_figs, subID == "sub_04" &
                                          trial_number_full == 32),
                                 aes(x = time, y = x_coord_new,
                                     color = item_type, shape = trial_type)) +
  #facet_wrap(~trial_number_full) +
  geom_point() +
  geom_vline(data = filter(data_mouse_breakpoint_figs, subID == "sub_04" &
                             trial_number_full == 32),
             aes(xintercept = time_min), lwd = 1.5) +
  scale_color_manual(values=c(col_noaccent, col_accent)) +
  ggtitle("Example Mouse Tracking with Break point") +
  xlab("Time in ms") +
  ylab("Distance from incorrect item") +
  theme_classic() +
  theme(text = element_text(size=18),
        legend.position = "top",legend.title = element_blank())

mouse_breakpoint_0.plot


